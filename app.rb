require 'pry'
require './models/cook_book'
require 'yaml'

cart = {}
recepies = YAML.load_file('./recepies/breakfast.yml')
# menu = YAML.load_file('./recepies/menu.yml')
xplan = YAML.load_file('./recepies/x-plan.yml')

xplan.each do |_day, meals|
  meals.each do |meal|
    if cart[meal]
      cart[meal] = cart[meal].merge(qty: (cart[meal][:qty] || 0) + 1)
    else
      if recepies[meal]
        cart[meal] = recepies[meal]
      else
        cart[meal] = {'ingredients' => { meal => 1 }}
      end
    end
  end
end

# menu.each {|recepie, qty| cart[recepie] = recepies[recepie].merge(qty: qty) }
puts CookBook.new(cart).cart
