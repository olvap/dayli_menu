# frozen_string_literal: true

require 'ruby-units'

class CookBook
  def initialize(recepies)
    @recepies = recepies
  end

  def ingredients
    @recepies.values.map do |recepie|
      recepie['ingredients'].map do |ingredient, unit|
        { ingredient => Unit.new(unit) * (recepie[:qty] || 1) }
      end
    end.flatten
  end

  def cart
    ingredients.reduce({}) do |memo, object|
      memo.merge(object) { |_key, oldval, newval| newval + oldval }
    end
  end
end
